/* array likeなデータ(配列に似たデータ構造)にforEachを適用させる関数
 */
const foreach = Array.prototype.forEach

/* resetボタンのクリック時の処理を定義
 * 全てのselect要素について、valueが空文字列のoption要素を選択状態にし、かつ有効状態(not disabled)にする
 * メッセージを非表示にする
 * */
document.getElementById('reset')
  // click時に実行される処理を追加
  .addEventListener('click', eventObj => {
    foreach.call(
      fetchSelectObjs(),
      selectObj => {
        // option要素の中でvalueが空文字列のものを選択状態(selected)にする
        foreach.call(
          selectObj.getElementsByTagName('option'),
          optionObj => {
            if (optionObj.value == '') optionObj.selected = true
          }
        )
        // select要素を有効状態(not disabled)にする
        selectObj.disabled = false
      }
    )
    document.getElementById('message').innerText = ""
  })

/* 全てのselect要素に選択時の処理を定義
 * 選択したselect要素を無効状態(disabled)にして、勝利判定を行う
 * どちらかのプレーヤーが勝った場合、全てのselect要素を無効状態にする
 * select要素の選択後、選択肢(option)を全て削除し、空白と選択しなかった方の記号の選択肢を追加する
 */
foreach.call(
  fetchSelectObjs(),
  selectObj => {
    selectObj.addEventListener('change', eventObj => {
      const selectObj = eventObj.target
      const pos = selectObj.name
      const row = pos.charAt(0)
      const column = pos.charAt(1)
      const figure = selectObj.value

      // select要素を無効状態(disabled)にする
      selectObj.disabled = true
      // 行、列、ななめで揃っているか判定
      if (ifRow(row, figure) || ifColumn(column, figure) || ifCross(pos, figure)) {
        // どちらが勝ったかメッセージに表示
        document.getElementById('message').innerText = figure + " is win!"
        // 全てのselect要素を無効状態(disabled)にする
        foreach.call(
          fetchSelectObjs(),
          selectObj => selectObj.disabled = true
        )
      }

      // 有効状態(not disabled)な全てのselect要素について
      // oが選択されたなら、選択肢を空白とxだけにする
      // xが選択されたなら、選択肢を空白とoだけにする
      foreach.call(
        fetchEnabledSelectObjs(),
        selectObj => {
          // 全ての子要素を削除
          selectObj.textContent = null
          // 要素を生成して子要素として追加
          selectObj.appendChild(document.createElement('option'))
          let optionObj = document.createElement('option')
          optionObj.value = optionObj.innerText = figure == 'o' ? 'x' : 'o'
          selectObj.appendChild(optionObj)
        }
      )
    })
  })

/* 全てのselect要素を取得
 * @return object
 */
function fetchSelectObjs() {
  return document.getElementsByTagName('select')
}

/* 有効状態(not disabled)の全てのselect要素を取得
 * @return object
 */
function fetchEnabledSelectObjs() {
  // filterは配列の中で、渡した関数の条件をクリアする要素のみ取り出した配列を返す
  return Array.prototype.filter.call(
    fetchSelectObjs(),
    selectObj => selectObj.disabled == false
  )
}

/* 列で揃っているかどうか判定する
 * @param  a      string  'a' or 'b' or 'c'
 * @param  figure string  'o' or 'x'
 * @return        boolean
 */
function ifRow(row, figure) {
  const val1 = document.getElementById(row + '1').value
  const val2 = document.getElementById(row + '2').value
  const val3 = document.getElementById(row + '3').value
  if (val1 == val2 && val2 == val3) return true
  return false
}

/* 行で揃っているかどうか判定する
 * @param  column string  '1' or '2' or '3'
 * @param  figure string  'o' or 'x'
 * @return        boolean
 */
function ifColumn(column, figure) {
  const val1 = document.getElementById('a' + column).value
  const val2 = document.getElementById('b' + column).value
  const val3 = document.getElementById('c' + column).value
  if (val1 == val2 && val2 == val3) return true
  return false
}

/* 斜めのラインで揃っているか判定する
 * @param  pos    string  'a1' or 'a2' or 'a3' or 'b1' or 'b2' or 'b3' or 'c1' or 'c2' or 'c3'
 * @param  figure string  'o' or 'x'
 * @return        boolean
 */
function ifCross(pos, figure) {
  if (pos == 'a1' || pos == 'b2' || pos == 'c3') {
    const val1 = document.getElementById('a1').value
    const val2 = document.getElementById('b2').value
    const val3 = document.getElementById('c3').value
  if (val1 == val2 && val2 == val3) return true
  } else if (pos == 'a3' || pos == 'b2' || pos == 'c1') {
    const val1 = document.getElementById('c1').value
    const val2 = document.getElementById('b2').value
    const val3 = document.getElementById('a3').value
    if (val1 == val2 && val2 == val3) return true
  }
  return false
}

